<?php

if( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/**
 * @wordpress-plugin
 * Plugin Name: Generation Filter Objects
 * Plugin URI: https://thegeneration.se/
 * Description: A plugin that lets you create your own metaboxes for your custom post type and deside on what parameters the filter will filter on from a settings page
 * Version: 1.0.0
 * Author: The Generation AB
 * Author URI: https://thegeneration.se
 * Text Domain: gen-filter-objects
 * Domain Path: languages
 */

/**
 * Define an absolute constant to be used in the plugin files
 */
if( ! defined( 'GEN_FILTER_OBJECTS_DIR' ) )
	define( 'GEN_FILTER_OBJECTS_DIR', __DIR__ );

if( ! defined( 'GEN_FILTER_OBJECTS_FILE' ) )
	define( 'GEN_FILTER_OBJECTS_FILE', __FILE__ );

/**
 * Require all the classes we need
 */
require_once GEN_FILTER_OBJECTS_DIR . '/inc/class-gen-filter-objects-i18n.php';
require_once GEN_FILTER_OBJECTS_DIR . '/inc/class-gen-filter-objects-scripts.php';
require_once GEN_FILTER_OBJECTS_DIR . '/inc/class-gen-filter-objects-settings.php';

/**
 * Initialize them
 */
(new Gen_Filter_Objects_i18n())->init();
(new Gen_Filter_Objects_Scripts())->init();

$plugin_description = __( 'A plugin that lets you create your own metaboxes for your custom post type and deside on what parameters the filter will filter on from a settings page', Gen_Filter_Objects_i18n::TEXT_DOMAIN );
$plugin_name = __( 'Generation Filter Objects', Gen_Filter_Objects_i18n::TEXT_DOMAIN );