<?php

if( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class Gen_Filter_Objects_i18n {

    const TEXT_DOMAIN = 'gen-filter-objects';

    /**
     * Init function
     */
    public function init() {
        add_action( 'plugins_loaded', array( $this, 'load_language_files' ) );
    }

    /**
     * Loads the language-files to be used throughout the plugin
     *
     * @return  void
     */
    public function load_language_files() {
        load_plugin_textdomain( self::TEXT_DOMAIN, false, plugin_basename( GEN_FILTER_OBJECTS_DIR ) . '/languages' ); 
    }

}
