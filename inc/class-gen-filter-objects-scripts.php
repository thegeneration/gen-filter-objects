<?php

if( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

class Gen_Filter_Objects_Scripts {

    /**
     * Init function
     */
    public function init() {
        add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_frontend_scripts' ) );
        add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_backend_scripts' ) );
    }

    /**
     * Enqueues scripts and styles for the frontend
     *
     * @return  void
     */
    public function enqueue_frontend_scripts() { 
        wp_enqueue_style( 'gen-filter-objects-frontend', plugins_url( 'assets/css/frontend/application.min.css', __DIR__ ) );
        wp_enqueue_script( 'gen-filter-objects-frontend', plugins_url( 'assets/js/frontend/application.min.js', __DIR__ ), array( 'jquery' ), true );
       
        
    }

    /**
     * Enqueues scripts and styles for the backend
     *
     * @return  void
     */
    public function enqueue_backend_scripts() { 
        wp_enqueue_style( 'gen-filter-objects-backend', plugins_url( 'assets/css/backend/application.min.css', __DIR__ ) );
    }
}