<?php
if (!defined('ABSPATH'))
    exit; // Exit if accessed directly

class Gen_Filter_Objects_Settings {

    const OPTIONS_NAME = 'gen_filter_objects_settings';
    const OPTIONS_SLUG = 'gen_filter_objects';

    /**
     * Construct-function, instantiates this class
     */
    public function __construct() {
        add_action('admin_menu', array($this, 'register_page'));
        add_action('admin_init', array($this, 'init_settings'));
    }

    public function register_page() {
        $page_title = __('Settings for Custom Post Type', Gen_Filter_Objects_i18n::TEXT_DOMAIN);
        $menu_title = __('CPT Settings', Gen_Filter_Objects_i18n::TEXT_DOMAIN);
        $capability = 'manage_options';
        $menu_slug = self::OPTIONS_SLUG;
        $callback = array($this, 'options_page');

        $hook = add_options_page($page_title, $menu_title, $capability, $menu_slug, $callback);
        add_action(sprintf('load-%s', $hook), array($this, 'update_options'));
    }

    /**
     * Function being run when options are updated
     *
     * @return void
     */
    public function update_options() {
        if (isset($_GET['settings-updated']) && $_GET['settings-updated']) {
            flush_rewrite_rules(false);
        }
    }

    public function init_settings() {
        register_setting(sprintf('%s-group', self::OPTIONS_NAME), self::OPTIONS_NAME, array($this, 'sanitize'));

        $section_name = "gen_filter_objects_settings_section";
        $section_callback = array($this, 'settings_section');
        $page_name = self::OPTIONS_SLUG;

        add_settings_section(
            $section_name, __('Settings for Custom Post Type', Gen_Filter_Objects_i18n::TEXT_DOMAIN), $section_callback, $page_name
        );
        
        add_settings_field( 'gen_filter_objects_cpt_title', __( 'Name of Custom Post Type', Gen_Filter_Objects_i18n::TEXT_DOMAIN ), array( $this, 'text' ), $page_name, $section_name, array(
            'name'          =>  sprintf( '%s[gen_filter_objects_cpt_title]', self::OPTIONS_NAME ),
            'value'         =>  self::get_option( 'gen_filter_objects_cpt_title', '' ),
            'placeholder'   =>  __( 'My custom post type', Gen_Filter_Objects_i18n::TEXT_DOMAIN ),
            'description'   => __( 'Choose the title of your CPT (Custom post type)', Gen_Filter_Objects_i18n::TEXT_DOMAIN ),
        ) );

    }
    
    public static function get_option( $name, $default = '' ) {
    	$settings = get_option( self::OPTIONS_NAME, array() );

    	if( ! isset( $settings[$name] ) ) {
    		return $default;
    	}

    	return $settings[$name];
    }
    
    /**
     * Sanitize each setting field as needed
     *
     * @param array $input Contains all settings fields as array keys
     * @return array Contains all the new setting fields as array keys
     */
    public function sanitize($input) {
        $settings = get_option( self::OPTIONS_NAME, array() );
        $new_input = array();
        
        $cpt_title = self::get_option( 'gen_filter_objects_cpt_title', '' );

        if( isset( $input['gen_filter_objects_cpt_title'] ) ) {
            $cpt_title = $input['gen_filter_objects_cpt_title'];
        }

        $new_input['gen_filter_objects_cpt_title'] = $cpt_title;
        return $new_input;
    }

    /**
     * Print the Section text
     */
    public function settings_section() {
        
    }

    public function options_page() {
        ?>
        <div class="wrap">
            <h1><?php echo _e('Product Settings', Gen_Filter_Objects_i18n::TEXT_DOMAIN); ?></h1>
            <form method="POST" action="options.php">
                <?php
                settings_fields(sprintf("%s-group", self::OPTIONS_NAME));
                do_settings_sections(self::OPTIONS_SLUG);
                submit_button();
                ?>
            </form>
        </div>
        <?php
    }

    public function text( $args ) {
        $name = esc_attr( $args["name"] );
        $value = $args["value"];
        $placeholder = isset( $args["placeholder"] ) ? $args["placeholder"] : '';
        $description = isset( $args["description"] ) ? $args["description"] : false;
        $size = isset( $args["size"] ) ? $args["size"] : 50;
        ?>
        <input type="text" name="<?php echo $name; ?>" size="<?php echo $size; ?>" value="<?php echo $value; ?>" placeholder="<?php echo $placeholder; ?>" />
        <?php if( $description !== false ) : ?>
        <p class="howto"><?php echo $description; ?></p>
        <?php endif; ?>
        <?php
    }
}

/**
 * Instantiate class when included
 */
new Gen_Filter_Objects_Settings;
